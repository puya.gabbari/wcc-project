function Tripslist(props:{trips:any[]}) {
    return <ul>
      {props.trips.map((t) => <li>Car with Id {t[0]} to {t[1]}</li>)}
    </ul>
} 
export default Tripslist;