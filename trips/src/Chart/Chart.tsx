import { Bar } from "react-chartjs-2";

function InputToChart(trip: string[]) {
    const counts: any = {};
    for (let i = 0; i < trip.length; i++) {
        counts[trip[i][1]] = 1 + (counts[trip[i][1]] || 0);
    }
    return counts
}

function setData(labels: string[], data: Number[]) {
    return {
        labels: labels,
        datasets: [{
            label: '# of Votes',
            data: data,
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
        }]
    }
}

const options = {
    
    scales: {
        yAxes: [{
            ticks: {
                beginAtZero: true,
            }
        }]
    }
}

function Chart(props: { trip: string[] }) {
    const data = InputToChart(props.trip)
    return <Bar data={setData(Object.keys(data), Object.values(data))} options={options} height={250}
    />
}

export default Chart 
