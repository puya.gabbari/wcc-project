import { ChangeEvent, useEffect, useState } from "react"
import {Button, TextField, List, ListItem, ListItemText, ListItemIcon, Checkbox, Container } from '@material-ui/core';
import { Add } from "@material-ui/icons";


function Tripsform(props:{onSubmit:any}) {
    const [car, setCar] = useState("")
    const [location, setLocation] = useState("")

    function submit(e:any) {
        e.preventDefault() 
        props.onSubmit([car, location])
        }
  
    function handleChange(e:ChangeEvent<HTMLInputElement>) {
      const name  = e.target.name
      if (name === 'car') {
          setCar(e.target.value)
      }
      if (name === 'location') {
          setLocation(e.target.value)
      }
  }
  
  return <form onSubmit={submit}>
  <div>
      <label>Car Identification
          <input type={"car"} name="car" onChange={handleChange}/>
      </label>
  </div>
  <div>
      <label>To
          <input type={"location"} name="location" onChange={handleChange}/>
      </label>
  </div>
  <div>
  <input type={"submit"} value="Call Car to Location" onClick={submit}></input>
  </div>
</form>
    
}

export default Tripsform;