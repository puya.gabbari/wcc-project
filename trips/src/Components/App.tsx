import React, {useEffect, useState } from "react"
import './App.css';
import Map from '../Map/Map';
import icon from "./icon.png";
import Chart from "../Chart/Chart";
import Tripsform from "../Chart/Tripsform";
import Tripslist from "../Chart/Tripslist";

interface Trip { car:string, location:string }


function App() {
  const [trips, setTrips] = useState([""])


  function loadTrips() {
    fetch("/trips")
    .then( response => response.json())
    .then( newTrip => setTrips(newTrip))
  }
    useEffect(loadTrips, [])

  function addTrip(trip:string){
    setTrips([...trips, trip])
    fetch("/trips", {
      method: "POST", headers: {
          'content-type': 'application/json;charset=UTF-8',
      },
      body: JSON.stringify(trip) })
  .then( data => console.log(data) )
  }


    return <div>
            <header/>
              <h1> <img src={icon} className="icon" width="70px"/>Fast Trips Dashboard</h1>
                <div id="list">
                <Tripslist trips={trips}/>
                </div>
                <div id="map" style={{width:"45%", display:"inline-block"}}>
                  <Map/>
                </div>
                <div id="chart" style={{width:"45%", display:"inline-block"}}>
                  <Chart trip={trips}/>
                  <Tripsform onSubmit={addTrip}/>
                </div>
              <footer> This page was created by Puya Gabbari - Student number: 44443.</footer>
              </div>
}
export default App;